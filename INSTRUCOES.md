<p align="center">
	<a href="http://www.brq.com/" title="BRQ Digital Solutions"><img height="80" src="http://www.brq.com/img/brq-digital-solutions.png"/></a>
	<a href="https://www.ccee.org.br/portal/faces/pages_publico/inicio" title="CCEE - Câmera de Comercialização de Energia Elétrica">
		<img height="80" src="http://www.abrapch.org.br/admin/arquivos//blog/grande/ccee-logo-120.png"/>
	</a>
	<a href="https://developer.oracle.com/java" title="Oracle Java"><img height="90" src="https://www.vectorlogo.zone/logos/java/java-card.png"/></a>
</p>

# Teste Prático de Java BRQ CCEE

Então você decidiu encarar este desafio... legal!!

Este teste está dividido em 2 etapas: uma de programação java e uma de análise.

Analise atenciosamente os cenários propostos e siga atentamente as intruções abaixo.


## Etapa 1: programação Java

### Cenário

> <p align="justify">Desenvolva um sistema para calcular o salário líquido dos funcionários de uma empresa.</p>


> <p align="justify">Tenha em mente que o salário líquido é igual ao salário bruto menos a soma de todos os descontos ( salário líquido = salário bruto - (soma dos descontos) ).</p>

> <p align="justify">O sistema deve apresentar a relação de funcionários ordenada por salário líquido de modo decrescente.</p>

> <p align="justify">Faça um diagrama de Classe e um diagrama de sequência.</p>

### Obsevações:

- O projeto já contém uma classe de conexões (br.brq.com.prova.jdbc.ConnectionFactory) configurada com os parâmetros do banco de dados;
- Não é permitido utilizar frameworks de persistência;
- Só é permitido utilizar as funcionalidades do JDBC e do JSE ( Java Second Edition );
- O sistema rodará em JSE, ou seja, não rodará em servidores de aplicações ( Ex: Jboss, Tomcat, Websphere, etc );
- Utilize os conceitos de Orientação a Objetos ( Beans ) e a API do Java para realizar as operações necessárias;
- O cálculo do valor líquido não poderá ser resolvido pela query do banco de dados;
- Há um arquivo chamado **teste_java.pdf** dentro deste repositório. Ele contém o modelo do banco de dados.

**Lembre-se: você está desenvolvendo um sistema orientado a objetos.**






## Etapa 2: análise

### Cenário

> <p align="justify">Uma corretora financeira, nova no mercado de ações deseja iniciar o projeto de desenvolvimento de portal para acesso aos clientes. Pretende que seja baseado nos principais padrões de softwares existentes no mercado.</p>

> <p align="justify">Através do portal da corretora o cliente poderá efetuar o seu cadastro. Após o cadastro, a corretora enviará um email de confirmação, informando dados para acesso.</p>

> <p align="justify">Cabe salientar que para esse cadastro, apenas alguns dados são necessários: CPF, código Bovespa, nome, email, usuário e senha. No momento da adesão do cliente, a corretora já coletou os demais dados e providenciou o código Bovespa do cliente.</p>

> <p align="justify">Tendo acesso ao portal o cliente poderá comprar / vender ações, visualizar a cotação de cada ação. Ele poderá também acessar sua carteira, obtendo informações de todos os investimentos que realizou, contendo a data/hora da operação, valor de compra da ação e quantidade adquirida.</p>

> <p align="justify">O valor das ações vendidas pelo cliente será disponibilizado em sua conta investimento, criada e administrada pela corretora no momento da adesão do cliente.</p>

### Intruções

1. Elabore o diagrama de Casos de Uso para a situação;
2. Selecione um Caso de Uso para elaboração da documentação descritiva que deverá ser desenvolvida a partir do template ( arquivo **template_especificacao_caso_de_uso.pdf** );



## Instruções para entrega:

- A partir da branch master, crie uma branch com a seguinte nomenclatura: **prova-brq-ccee-NOME-SOBRENOME** e trabalhe SOMENTE em sua branch;
- Após finalizar o desenvolvimento, faça um push de sua branch para o gitlab;
- Se achar necessário crie um arquivo README para indicar a forma de uso de seu código ;
- Os diagramas e casos de uso devem ser salvos na branch.

# Resolução

## Diagrama de Classe
https://github.com/arilsonsantos/ccee/blob/master/diagramas/diagrama-de-classe.pdf
![ScreenShot](https://github.com/arilsonsantos/ccee/blob/master/diagramas/diagrama-de-classe.png)

## Diagrama de Sequência
https://github.com/arilsonsantos/ccee/blob/master/diagramas/diagrama-de-sequencia.pdf
![ScreenShot](https://github.com/arilsonsantos/ccee/blob/master/diagramas/diagrama-de-sequencia.png)


## Diagrama de Casos de Uso
https://github.com/arilsonsantos/ccee/blob/master/diagramas/diagrama-de-casos-de-uso.pdf
![ScreenShot](https://github.com/arilsonsantos/ccee/blob/master/diagramas/diagrama-de-casos-de-uso.png)

## Especificação - Caso de Uso Cadastrar
https://github.com/arilsonsantos/ccee/blob/master/diagramas/especificacao-caso-de-uso-cadastrar.pdf
![ScreenShot](https://github.com/arilsonsantos/ccee/blob/master/diagramas/especificacao-caso-de-uso-cadastrar.PNG)

# Breves instruções para download, compilação e execução

### Necessário ter o Maven configurado (https://maven.apache.org/download.cgi)
### Acessar a URL https://git.brq.com/ccee/ccee-prova-java/tree/prova-brq-ccee-ARILSON-SANTOS
### Efertuar o download (Download zip)
### Acessar o diretório ccee-prova-java-prova-brq-ccee-ARILSON-SANTOS 
### Digitar mvn install
### Digitar java -jar target/prova-java-1.0.0-jar-with-dependencies.jar



