# Resolução do teste aplicado pela BRQ

Instruções do teste -> https://git.brq.com/ccee/ccee-prova-java/blob/prova-brq-ccee-ARILSON-SANTOS/README.md


## Diagrama de Classe
https://github.com/arilsonsantos/ccee/blob/master/imagens/diagramas/diagrama-de-classe.pdf
![alt text](imagens/diagramas/diagrama-de-classe.png)

## Diagrama de Sequência
https://github.com/arilsonsantos/ccee/blob/master/imagens/diagramas/diagrama-de-sequencia.pdf
![alt text](imagens/diagramas/diagrama-de-sequencia.png)


## Diagrama de Casos de Uso
https://github.com/arilsonsantos/ccee/blob/master/diagramas/imagens/diagrama-de-casos-de-uso.pdf
![alt text](imagens/diagramas/diagrama-de-casos-de-uso.png)

## Especificação - Caso de Uso Cadastrar
https://github.com/arilsonsantos/ccee/blob/master/imagens/diagramas/especificacao-caso-de-uso-cadastrar.pdf
![alt text](imagens/diagramas/especificacao-caso-de-uso-cadastrar.PNG)

## Breves instruções para download, compilação e execução

##### Necessário ter o Maven configurado (https://maven.apache.org/download.cgi)
##### Acessar a URL https://git.brq.com/ccee/ccee-prova-java/tree/prova-brq-ccee-ARILSON-SANTOS
##### Efertuar o download (Download zip)
![alt text](imagens/botao-download.png)
![alt text](imagens/download-zip.png)

##### Acessar o diretório ccee-prova-java-prova-brq-ccee-ARILSON-SANTOS 

##### Digitar mvn install

##### Digitar java -jar target/prova-java-1.0.0-jar-with-dependencies.jar

#### Exemplo de uma execução:
![alt text](imagens/exemplo-execucao.png)



