package br.com.brq.prova.entity;

import java.math.BigDecimal;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Funcionario
 * @author Arilson Santos
 * @since 19/05/2019
 * @version 1.0
 */
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class Funcionario {

    private Long id;
    private String nomeCliente;
    private BigDecimal valorSalarioBruto;

    public Funcionario(Long id) {
        this.id = id;
    }

    public String toString() {
        return "[" + "ID DO FUNCIONARIO = " + getId() + ",NOME DO CLIENTE = " + getNomeCliente() + "'"
                + ",VALOR SALÁRIO BRUTO = " + getValorSalarioBruto() + "]";
    }

}