package br.com.brq.prova.service;

import java.util.List;

import br.com.brq.prova.dao.FuncionarioDao;
import br.com.brq.prova.entity.Funcionario;
import br.com.brq.prova.jdbc.ConnectionFactory;

/**
 * FuncionarioService
 */
public class FuncionarioService {

    public List<Funcionario> findAll() {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        FuncionarioDao funcionarioDao = new FuncionarioDao(connectionFactory);

        return funcionarioDao.findAll();
    }
}