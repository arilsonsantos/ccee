package br.com.brq.prova.service;

import java.util.List;

import br.com.brq.prova.entity.dto.FuncionarioSalarioLiquidoDto;

/**
 * ICalculoDesconto
 * @author Arilson Santos
 * @since 19/05/2019
 * @version 1.0
 */
public interface ICalculoDesconto {

    List<FuncionarioSalarioLiquidoDto> executar(AbstractCalculoDesconto calculoDesconto);
}