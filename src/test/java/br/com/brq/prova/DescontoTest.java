package br.com.brq.prova;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;

import br.com.brq.prova.dao.DescontoDao;
import br.com.brq.prova.entity.Desconto;
import br.com.brq.prova.jdbc.ConnectionFactory;

/**
 * FuncionarioDaoTest
 * @author Arilson Santos
 * @since 19/05/2019
 */
public class DescontoTest {

    @Test
    public void testFindAll() {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        DescontoDao descontoDao = new DescontoDao(connectionFactory);
        List<Desconto> descontos = descontoDao.findAll();

        assertThat(descontos, notNullValue());
    }
}